# Auto Version Script for Xcode

AutoVersion is a script you put in your Xcode project's "Build Phase" Run Script section which will automatically build a version file for your app each time you build your app.  Include version.swift (or the older version.h if you're using ObjectiveC) in your app. This file contains code-readable values with your app version, name, copyright, and environmental information such as SDK and Xcode versions.  You can use this for logging or for user display as you need. Do not edit the version file, but edit your project file to change those version and copyright values.

This file can also automatically update the copyright year when the year changes.  This means that versioning and copyright both get easier.

There are two scripts, one for Objective-C and another for Swift.  They generate equivalent output in a file named version.h (for ObjC) or version.swift (Swift).  The file that they generate is rebuilt every build by the script so it should not be stored in git. (The new changes to update the copyright year are only in the Swift version.)

### Note:
As of 2021, the Objective C version is no longer being maintained. Only the Swift version is receiving updates. Also the Objective C script doesn't have the copyright feature.

### Changes:
July 2021: Updated to include Xcode 13 beta changes
July 2021: Updated to remove beta, alpha markers from version itself. Added a RELEASE_TAG user-defined var in the Build Settings for that. This is to be better compatible with TestFlight and the app store.
Apr 2023: Simplifed the Swift file, added sample gitignore file.

## Installation

In your Xcode project do the following:

* Select your project's target you want to add the script to, then select the "Build Phases" tab.
* Create a new "Run Script" phase with the "+" sign at the top left of the pane.
* Position your new run script phase immediately after "Target Dependencies" phase.  This script needs to run before the Compile Sources phase.
* Copy and paste the script contents from the Swift or ObjC version as appropriate for your project.
* Change the default shell from "/bin/sh" to "/bin/bash" at the top of the Run Scripts pane.  (This script uses some bash-only matching operations.) 
* In your Build Settings in Xcode, use the "+" to the left of the search field to add the user-define variables COPYRIGHT_NAME, COPYRIGHT_YEAR, and if you want it, RELEASE_TAG
* Run the build once to make the version file, then include it in your project.
* Add the version file to your .gitignore to make sure you do not archive the version.h or version.swift file in your git repository. This file is generated for you each time you build your application. (See sample gitignore file.)


## Use

These are some notes on some ways to use the auto version scripts in your project.  I cover the items the script generates and how they can be used.

### Copyright

Most of the script is concerned with the app or build environment versions.  But the script also can help make the copyright available in your code.   The auto version scripts will look for `COPYRIGHT_NAME` and `COPYRIGHT_YEAR` variables in your build environment. These have to be added by you as user-defined settings in your project's "Build Settings". If you're using copyright year updating then the COPYRIGHT_YEAR is the *FIRST* year of your copyright. Otherwise it's the copyright year that will be shown. These user-defined build settings are as in the following example:
<pre>
COPYRIGHT_YEAR = 2021
COPYRIGHT_NAME = Malcolm Appleseed
RELEASE_TAG = beta 1
</pre>

These are optional.  If you don't use them, then the eqivalent strings in the version file will be empty. If you don't use RELEASE_TAG, then there'll be no release tag name in the version. This is what you want for your final release actually.

If you want to use automatic copyright updating, then make sure that the line "CopyrightUpdating=1" is NOT commented out. This will make the copyright of the form "2014-2021" if your COPYRIGHT_YEAR is 2014 and the current year is 2021. Comment out this line in the script to turn off this feature.

### Versions

This script supports using a git code repository and using [semantic versioning](http://semver.org) version numbering. RELEASE_TAG user-define build setting can be used to add a "beta 1" or whatever name you need in the version. For a final GA release version, remove the contents of the RELEASE_TAG.

Set the version and build number in the "General" tab of your target in your project file in the "Identity" section at the top.  This automatically sets these values in your plist file. 

In this example the app is 1.0.0 version, beta 1, build 1, with git hash `0B13E` and is dirty, ie there are code changes that are not yet checked into the repository.  This FullVersion is good for logging, but you might not way to expose it to end users in a release version.  However it's very nice to know exactly what build and what git commit match this build and version of your app!

So the auto version script breaks down this FullVersion into components with the following names:
* SimpleVersion: `1.0.0 beta 1`
* Version: `1.0.0(1) beta 1`
* BuildNumber: `1`
* BuildHash: `0B13E-dirty`

### Configuration

The configuration items cover the configuration of the build (Develop or Release), the Xcode and SDK versions, and the minimum deployment target.  These are from environment variables from the build environment.  

It's useful to log these in your app or to report them to help you determine the user base you're on, and where problems many be found.  Perhaps they're specific to a particular SDK or Xcode version?  I would *not* use these to determine if an API is available or not. This is intended for logging and tracking.
